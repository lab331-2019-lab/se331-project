import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';


import { BehaviorSubject } from 'rxjs';
import { WaitingTableDataSource  } from './waiting-table-datasource';
import { AppService } from '../service/app-service';
import { Student } from '../entity/student';

@Component({
  selector: 'app-waiting',
  templateUrl: './waiting.component.html',
  styleUrls: ['./waiting.component.css']
})
export class WaitingComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: WaitingTableDataSource ;


  displayedColumns = ['id','email','name','surname','birthday','image','confirm','reject'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  disabled: boolean[] = [];

  name: string;
  surname: string;
  image: string;
  birthday: any;
  studentId: any;
  email: string;
  password: string;
  activities: any[];
  constructor(private appService: AppService) {

  }
  ngOnInit() {

    this.appService.getWaiting().subscribe(data => {
      this.students = data.map(e => {
        
        return {
          name: e.payload.doc.data()['name'],
          surname: e.payload.doc.data()['surname'],
          birthday: e.payload.doc.data()['birthday'],
          email: e.payload.doc.data()['email'],
          image: e.payload.doc.data()['image'],
          password: e.payload.doc.data()['password'],
          activities: e.payload.doc.data()['activities'],
          studentId: e.payload.doc.data()['studentId'],
          id: e.payload.doc.id
          
        };
      });
    
      this.dataSource = new WaitingTableDataSource ();
      this.dataSource.data = this.students;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;

      for (let i = 0; i < this.students.length; i++) {
        this.disabled[i] = false;
      }
    });

  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  confirm(student) {
   
    this.appService.confirm(student.id); 
    console.log(student);
    let record = {};
    record['name'] = student.name;
    record['surname'] = student.surname;
    record['image'] = student.image;
    record['birthday'] = student.birthday;
    record['studentId'] = student.studentId;
    record['email'] = student.email;
    record['password'] = student.password;
    record['activities'] = [];

    this.appService.addStudent(record).then(resp => {
      this.name = "";
      this.surname = "";
      this.image = "";
      this.birthday = "";
      this.studentId = "";
      this.email = "";
      this.password = "";
      this.activities = [];
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }

  reject(student){
    this.appService.confirm(student.id);
  }


}
