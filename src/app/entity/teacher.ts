import { Activity } from './activity';

export class Teacher {
    name: string;
    surname: string;
    email: string;
    password: string;
    activities: Activity [];
}
