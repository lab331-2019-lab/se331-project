import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge, BehaviorSubject } from 'rxjs';




const EXAMPLE_DATA: any[] = []

export class EnrolledTableDataSource extends DataSource<any> {
    data: any[] = EXAMPLE_DATA;
    paginator: MatPaginator;
    sort: MatSort;
    filter$: BehaviorSubject<string>;
    constructor() {
        super();
    }


    connect(): Observable<any[]> {

        const dataMutations = [
            observableOf(this.data),
            this.paginator.page,
            this.sort.sortChange,
            this.filter$.asObservable()
        ];

        this.paginator.length = this.data.length;
        return merge(...dataMutations).pipe(map(() => {
            return this.getFilter(this.getPagedData(this.getSortedData([...this.data])));
        }));
    }


    disconnect() { }


    private getPagedData(data: any[]) {
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    }


    private getSortedData(data: any[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);


                default: return 0;
            }
        });
    }

    private getFilter(data: any[]): any[] {
        const filter = this.filter$.getValue();
        if (filter === '') {
            return data;
        }

        return data.filter((activity) => {
            return (activity.name.toLowerCase().includes(filter)  || activity.host.toLowerCase().includes(filter));
        });
    }
}

function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
